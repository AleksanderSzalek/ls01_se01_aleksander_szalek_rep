﻿import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);

        String Antwort;
        do {
        int anzahlTickets;
        double zuZahlenderBetrag;
        double TicketPreis = 0;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double rueckgabebetrag;
        double eingegebenerBetrag;
        
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
        System.out.print("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n");
        System.out.print("  Tageskarte Regeltarif AB [8,60 EUR] (2)\n");
        System.out.print("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
        
        System.out.print("\nIhre Wahl: ");
        
        int wahl = myScanner.nextInt();
        
        int n = 4;
        
        if (n <= wahl) {
        	
        	System.out.println("falsche Eingabe");
            System.out.print("Ihre Wahl: ");
            
            wahl = myScanner.nextInt();
        	
        }

        if (wahl == 1) {
        	TicketPreis = 2.90;
        }
		if (wahl == 2) {
			TicketPreis = 8.60;
		}
		if (wahl == 3) {
			TicketPreis = 23.50;
		}
       
        
        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();

        zuZahlenderBetrag = TicketPreis * anzahlTickets;

        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f €%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rueckgabebetrag > 0.0) {
            //***** Lösung der Ausgabenformatierungsaufgabe *****************/
            System.out.format("Der Rückgabebetrag in Höhe von %4.2f € %n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\2n" + "vor Fahrtantritt entwerten zu lassen!"
                + "Wir wünschen Ihnen eine gute Fahrt.\n");
        
        System.out.println("Möchten Sie einen neuen Ticket kaufen? J/N");
        Antwort = tastatur.next();
        }
        while (Antwort.equals("J"));
    }
}