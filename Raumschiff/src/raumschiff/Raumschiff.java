package raumschiff;
import ladung.Ladung;
import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    private static ArrayList<String> boradcastKommunikator = new ArrayList<>();
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();

    static Random rand = new Random();

    public Raumschiff () {}
    public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
                      int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl,
                      String schiffname)
    {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
        this.schiffsname = schiffname;
    }

    public final int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }
    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public final int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }
    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public final int getSchildeInProzent() {
        return schildeInProzent;
    }
    public void setSchildeInProzent(int schildeInProzent) {
        if (schildeInProzent > 100) {
            this.schildeInProzent = 100;
        } else {
            this.schildeInProzent = schildeInProzent;
        }
    }

    public final int getHuelleInProzent() {
        return huelleInProzent;
    }
    public void setHuelleInProzent(int huelleInProzent) {
        if (huelleInProzent > 100) {
            this.huelleInProzent = 100;
        } else {
            this.huelleInProzent = huelleInProzent;
        }
    }

    public final int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }
    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        if (lebenserhaltungssystemeInProzent > 100) {
            this.lebenserhaltungssystemeInProzent = 100;
        } else {
            this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        }
    }

    public final int getAndroidenAnzahl() {
        return androidenAnzahl;
    }
    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public final String getSchiffname() {
        return schiffsname;
    }
    public void setSchiffname(String schiffname) {
        this.schiffsname = schiffname;
    }

    public static ArrayList<String> getBoradcastKommunikator() {
        return boradcastKommunikator;
    }
    public static void setBoradcastKommunikator(ArrayList<String> boradcastKommunikatorTemp) {
        boradcastKommunikator = boradcastKommunikatorTemp;
    }

    public final ArrayList<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }
    public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
        this.ladungsverzeichnis = ladungsverzeichnis;
    }
    public void addLadung (Ladung neueLadung) {

        this.ladungsverzeichnis.add(neueLadung);
    }
    public void ladungsverzeichnisAusgeben () {
        for (Ladung lad : ladungsverzeichnis) {
            lad.ausgabeLadung();
        }
    }
    public void zustandDesRaumschiffs () {
        System.out.println("Schiffsname: " + schiffsname);
        System.out.println("Anzahl der Photonentorpedo: " + photonentorpedoAnzahl);
        System.out.println("Energieversorgung: " + energieversorgungInProzent + " %");
        System.out.println("Schilde - Status: " + schildeInProzent + " %");
        System.out.println("H�lle - Status: " + huelleInProzent + " %");
        System.out.println("Lebenserhaltungssysteme - Status: " + lebenserhaltungssystemeInProzent + " %");
        System.out.println("Anzahl der Androiden: " + androidenAnzahl);
    }
    public void torpedosAbschiessen (Raumschiff r) {
        if (photonentorpedoAnzahl > 0) {
            nachrichtAnAlle("Photonentorpedo abgeschossen");
            treffer(r);
            --photonentorpedoAnzahl;
        } else {
            nachrichtAnAlle("=*Click*=");
        }
    }
    public void kanonenAbschiessen (Raumschiff r) {
        if (energieversorgungInProzent > 50) {
            nachrichtAnAlle("Phaserkanone abgeschossen");
            treffer(r);
            energieversorgungInProzent /= 2;
        } else {
            treffer(r);
            nachrichtAnAlle("=*Click*=");
        }
    }

    private void treffer (Raumschiff r) {
        System.out.println(r.getSchiffname() + " wurde getroffen");
        if (r.getSchildeInProzent() > 0) {
            r.setSchildeInProzent(r.getSchildeInProzent() / 2);
        } else {
            if (r.getHuelleInProzent() < 0) {
                r.setLebenserhaltungssystemeInProzent(0);
                nachrichtAnAlle("Lebenserhaltungssystem wurde vernichtet.");
            } else {
                r.setHuelleInProzent(r.getHuelleInProzent() / 2);
                r.setLebenserhaltungssystemeInProzent(r.getEnergieversorgungInProzent() / 2);
            }
        }
    }

    public void nachrichtAnAlle (String message) {
        System.out.println(message);
        boradcastKommunikator.add(message);
    }
    public static ArrayList<String> eintraegeLogbuchZurueckgeben () {
        return boradcastKommunikator;
    }

    public void ladungsverzeichnisAufraeumen () {
        for (Ladung ld : ladungsverzeichnis) {
            ladungsverzeichnis.removeIf(c -> c.getMenge() <= 0);
        }
    }
}
