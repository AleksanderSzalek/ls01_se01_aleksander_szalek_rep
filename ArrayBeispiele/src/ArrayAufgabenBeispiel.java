import java.util.Scanner;

public class ArrayAufgabenBeispiel {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Wieviel Zahlen möchten sie eingeben ");
		int anzahl = tastatur.nextInt();
		
		int[] zliste = new int[anzahl];
		
		for(int i=0; i<anzahl; i++) {
			System.out.println((i+1) +". Zahl:");
			zliste[i] = tastatur.nextInt();
		}
	
		System.out.print("[");
		
		
		for(int i=0; i<anzahl; i++) {
			System.out.print(zliste[i]+" ");
			
	
		}
		System.out.print("]");
	}

}
