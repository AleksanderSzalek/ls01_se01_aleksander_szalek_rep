import java.util.Scanner;

public class Aufgabe1 {

	public static void main(String[] args) {
	
		System.out.println("Aufgabe 1");
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte die 1. Zahl ein:");
		int zahl1 = myScanner.nextInt();
		
		System.out.println("Geben Sie bitte die 2. Zahl ein:");
		int zahl2 = myScanner.nextInt();
		
		System.out.println("Geben Sie bitte die 3. Zahl ein:");
		int zahl3 = myScanner.nextInt();
		
		gebeGroesteZahlAus(zahl1, zahl2, zahl3);

	}

	
	public static void gebeGroesteZahlAus(int z1, int z2, int z3) {
		if ( z1 > z2 && z1 > z3 ) {
			System.out.println("Die gr��te Zahl lautet " + z1);
		}
		else {
			if ( z2 > z1 && z2 > z3 ) {
				System.out.println("Die gr��te Zahl lautet " + z2);
			}
			else {
				System.out.println("Die gr��te Zahl lautet " + z3);
			}
		}
	}
}
